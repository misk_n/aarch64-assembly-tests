.text 			    // code section
.globl hello_world_test
.globl my_strlen
.globl my_strcat

hello_world_test:
	mov x0, 1 	    // stdout has file descriptor 1
	ldr x1, =msg 	// buffer to write
	mov x2, msg_len	// size of buffer
	mov x8, 64 	    // sys_write() is at index 64 in kernel functions table
	svc #0 		    // generate kernel call sys_write(stdout, msg, len);
	mov x0, 0       // return 0
	ret

my_strlen:
    // parameter x0 is the pointer to the string
    // x1: counter
    mov x1, 0
.loop0:
    ldrb w8, [x0, x1]
    cmp w8, 0
    b.eq .end0
    add x1, x1, 1
    b .loop0
.end0:
    mov x0, x1
    ret

my_strcat:
    // parameter x0: dst
    // parameter x1: src (const)
    stp x19, x30, [sp, #-16]! // save link registers before call
    stp x0, x1, [sp, #-16]!   // save parameters
    bl my_strlen              // length of the first string is in x0
    ldp x1, x2, [sp], #16     // restore parameters in x1 and x2
    ldp x19, x30, [sp], #16   // restore link registers
    mov x3, 0                 // iterator
.loop1:
    ldrb w8, [x2, x3]         // load the char at x2[x3] into w8
    strb w8, [x1, x0]         // store w8 at x1[x0]
    cmp w8, 0
    b.eq .end1                // end condition: the char is null
    add x0, x0, 1             // increment counters x0 and x3
    add x3, x3, 1
    b .loop1
.end1:
    mov x0, x1                // the return value is the destination string
    ret

.data			    // data section
msg:
.ascii "Hello world\n"

msg_len = . - msg
