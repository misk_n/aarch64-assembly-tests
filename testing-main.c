//
// Created by nicolas on 9/19/18.
//

# include <stdio.h>

int hello_world_test();
int my_strlen(const char* str);
char *my_strcat(char *dst, const char *src);

int main()
{
    int test = hello_world_test();
    printf("hello world return value: %d\n", test);

    char *src = "this is a test";
    int len = my_strlen(src);
    printf("string length: %d\n", len);

    char dst[20] = "test\0";
    char *cat = my_strcat(dst, src);
    printf("strcat result: %s\n", cat);

    return 0;
}